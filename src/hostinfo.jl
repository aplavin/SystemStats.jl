struct HostInfo
    host::String
    clock_ticks::Int
    files::FileTree
end

Base.broadcastable(hi::HostInfo) = Ref(hi)
Base.gethostname(hi::HostInfo) = hi.host
root(hi::HostInfo) = hi.files
clock_ticks(hi::HostInfo) = hi.clock_ticks
nfilelines(hi::HostInfo) = nfilelines(root(hi))
nfilebytes(hi::HostInfo) = nfilebytes(root(hi))

users_by_uid(x) = etcpasswd_dict(x)
etcpasswd_dict(hi::HostInfo; kwargs...) = etcpasswd_dict(readlines(root(hi) / "/etc/passwd"); kwargs...)
etcpasswd_dict(lines::Vector{<:AbstractString}; by=x -> x.uid) = @p lines |> etcpasswd_table() |> groupmap(by, only) |> pairs |> Dict
etcpasswd_table(lines::Vector{<:AbstractString}) = @p lines |>
	map(NamedTuple{(:name, :passwd, :uid, :gid, :comment, :home, :shell)}(split(_, ':'))) |>
	mapset(uid=parse(Int, _.uid), gid=parse(Int, _.gid))



struct LocalhostRetriever end
struct SSHRetriever
    host::String
end

default_retriever(host::String) = host == "localhost" ? LocalhostRetriever() : SSHRetriever(host)

function retrieve_host_info(host::String, retriever=default_retriever(host))
    HostInfo(
        host,
        retrieve_clock_ticks(retriever),
        retrieve_files(retriever, ["/proc/stat", "/proc/meminfo", "/proc/*/{status,stat,cmdline}", "/etc/passwd"]),
    )
end

function retrieve_clock_ticks(retr)
    str = read_cmd(retr, "getconf CLK_TCK")
    return parse(Int, str)
end

function retrieve_files(retr, files)
    cmd = "grep -s --text '.' $(join(files, ' ')) || true"
    @p begin
        read_cmd(retr, cmd)
        split(__, '\n')
        filter(!isempty(_))
        map(split(_, ':'; limit=2))
        map((file=_[1], line=_[2]))
        IterTools.groupby(_.file)
        map(gr -> first(gr).file => @p gr |> map(_.line))
        Dict
        FileTree(__, "")
    end
end

read_cmd(::LocalhostRetriever, cmd::String) = read(`bash -c $cmd`, String)
read_cmd(r::SSHRetriever, cmd::String) = read(`ssh -C $(r.host) $cmd`, String)
