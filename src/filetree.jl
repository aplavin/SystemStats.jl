struct FileTree
    files_lines::Dict{String, Vector{SubString{String}}}
    path::String
end

Base.isfile(d::FileTree) = haskey(d.files_lines, d.path)

Base.:(/)(d::FileTree, p::String) = joinpath(d, p)
function Base.joinpath(d::FileTree, p::String)
    @set d.path = joinpath(d.path, p)
end

Base.readdir(d::FileTree) = @p begin
    keys(d.files_lines)
    collect
    filter(startswith(_, d.path))
    map(_[begin+length(d.path):end])
    map(splitpath(_)[1])
    unique
end

function Base.readlines(d::FileTree)
    @assert isfile(d)  d.path
    return d.files_lines[d.path]::Vector{<:AbstractString}
end

Base.read(d::FileTree, ::Type{String}) = join(readlines(d), '\n')

nfilelines(d::FileTree) = @p d.files_lines |> values |> sum(length(_))
nfilebytes(d::FileTree) = @p d.files_lines |> values |> sum(sum(length, _))

