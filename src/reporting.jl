function num2bar(num; rpad=0)
	num < 0 && return ""
	EIGHTS = Dict(0 => ' ', 1 => '▏', 2 => '▎', 3 => '▍', 4 => '▌', 5 => '▋', 6 => '▊', 7 => '▉', 8 => '█')
	Base.rpad(rstrip(EIGHTS[8] ^ floor(Int, num) * EIGHTS[ceil(Int, (num % 1) * 8)], ' '), rpad)
end

struct UtilizationReport
	hi_a::HostInfo
	hi_b::HostInfo
end

utilization_report((hi_a, hi_b)) = (@assert gethostname(hi_a) == gethostname(hi_b); UtilizationReport(hi_a, hi_b))

function print_cpu_usage(io, frac, crayon)
    str = f"{frac*100:4d}%"
    print(io,
        crayon(str[1:2]),
        Crayon(foreground=250)(str[3:end]),
        crayon(f" {num2bar(frac)}"),
    )
end

print_colored(x) = print_colored(stdout, x)
function print_colored(io::IO, ur::UtilizationReport)
	(;hi_a, hi_b) = ur
    nlines = 0
    function println(io, x...)
        Base.print(io, x..., clearline_end())
        Base.println(io)
        nlines += 1
    end

	println(io, Crayon(bold=true, foreground=:black, background=:light_gray)(f"{cpad(hi_a.host, 45)}"))

    let mem = meminfo(hi_b)
        # same as htop
        free_u = mem.free + mem.buffers + mem.cached
        used_u = mem.total - free_u
        barwidth = 45 - 14 - 1 - 2
        print(io,
            Crayon(foreground=:cyan)(f"{used_u/2^20:3.0f}G/{mem.total/2^20:3.0f}G"),
            " ",
            Crayon(foreground=:light_cyan)("[$(num2bar(barwidth * (used_u / mem.total); rpad=barwidth))]"),
        )
    end

    let ncpu = length(cputimes(hi_b).individual)
        diff = cputimes(hi_b).total - cputimes(hi_a).total
        used_frac = used_time(diff) / total_time(diff) * ncpu
        print_cpu_usage(io, used_frac, Crayon(foreground=152))
    end

    println(io)

	users = users_by_uid(hi_a)
    cpuload = cpu_load_between(hi_a, hi_b)
	proclines = @p begin
		cpuload.processes
		group((;_.uid, _.name))
		map((cnt=length(_), used_frac=sum(:used_frac, _), cmdlines=:cmdline.(_) |> unique))
		pairs
		collect
		map(merge(_[1], _[2]))
		sort(by=_.used_frac, rev=true)
        
        enumerate()
        collect()
		filter(_[1] <= 5 || _[2].used_frac > 0.3)
        map(_[2])

		foreach() do r
			print(io,
                Crayon(foreground=:green)(f"{users[r.uid].name:>15s}"),
                f"{first(r.name, 19):>20s}",
                Crayon(foreground=:light_blue)(lpad(r.cnt > 1 ? f"×{r.cnt:d}" : "", 4)),
                " "
            )
            print_cpu_usage(io, r.used_frac, Crayon(foreground=:light_magenta))
            println(io)
		end
	end
	println(io)
    return (;nlines)
end

function monitor_continously(hosts; interval=2)
    his₀ = asyncmap(retrieve_host_info, hosts)
    nlines = 0
    try
        while true
            sleep(interval)
            his₁ = asyncmap(retrieve_host_info, hosts)
            print(cursor_up(nlines))
            nlines = 0
            for (hi₀, hi₁) in zip(his₀, his₁)
                r = print_colored(stdout, utilization_report(hi₀ => hi₁))
                nlines += r.nlines
            end
            print(clearscreen_end())
            his₀ = his₁
        end
    catch exc
        exc isa InterruptException || rethrow()
        @info "Interrupt received"
    end
end

cursor_up(lines::Int) = "\x1b[999D\x1b[$(lines)A"
clearscreen_end() = "\x1b[0J"
clearline_end() = "\x1b[0K"

cpad(s::String, n::Integer) = rpad(lpad(s, (n + textwidth(s)) ÷ 2), n)
