module SystemStats

export 
    retrieve_host_info, root, users_by_uid,
    cputimes, used_time, idle_time, total_time,
    processes_info, meminfo

using DataManipulation
using Accessors
using PyFormattedStrings
import IterTools
using Crayons


include("filetree.jl")
include("hostinfo.jl")
include("loaddetails.jl")
include("reporting.jl")

end
