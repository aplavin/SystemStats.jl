
struct CpuTimes{T <: NamedTuple}
    fields::T
end

Base.getindex(cpu::CpuTimes, i::Symbol) = cpu.fields[i]
used_time(cpu::CpuTimes) = sum(delete(cpu.fields, @optic(_.idle)))
idle_time(cpu::CpuTimes) = cpu.fields[:idle]
total_time(cpu::CpuTimes) = sum(cpu.fields)
Base.:(-)(a::CpuTimes, b::CpuTimes) = CpuTimes(map(-, a.fields, b.fields))

Base.show(io::IO, ::MIME"text/plain", ct::CpuTimes) = print(io, "CpuTimes", ct.fields)
Base.show(io::IO, ct::CpuTimes) = print(io, "CpuTimes", ct.fields)

function cputimes(hi::HostInfo)
    ks = (:user, :nice, :system, :idle, :iowait, :irq, :softirq, :steal, :guest, :guest_nice)
    @p begin
        root(hi) / "/proc/stat"
        readlines
        filtermap(match(r"^cpu(?<cpu_id>\d*)\s+(?<fields>.+)$", _))
        map((;
            cpu=tryparse(Int, _[:cpu_id]),
            times=@p begin
                split(_ꜛ[:fields])
                map(parse(Int, _) / clock_ticks(hi))
                NamedTuple{ks}()
                CpuTimes()
            end
        ))
        @aside @assert isnothing(__[1].cpu)
        (
            total=__[1].times,
            individual=:times.(__[2:end])
        )
    end
end


function meminfo(hi::HostInfo)
    dct = @p begin
        root(hi) / "/proc/meminfo"
        readlines
        map(split(_, r": +"))
        map(_[1] => endswith(_[2], " kB") ? parse(Int, _[2][1:end-3]) : parse(Int, _[2]))
        Dict
    end
    (
        total = dct["MemTotal"],
        free = dct["MemFree"],
        buffers = dct["Buffers"],
        cached = dct["Cached"] + dct["SReclaimable"],
        shared = dct["Shmem"],
        active = dct["Active"],
        inactive = dct["Inactive"],
        available = dct["MemAvailable"],
    )
end

pids(hi::HostInfo) = @p begin
    root(hi) / "/proc/"
    readdir()
    map(tryparse(Int, _))
    filter(!isnothing)
    map(identity)
    sort
end

processes_info(hi::HostInfo) = @p pids(hi) |> filtermap(process_info(hi, _))

function process_info(hi::HostInfo, pid::Int)
    dir = root(hi) / "/proc/$pid/"
    r = let
        f = dir / "status"
        isfile(f) || return nothing
        dct = @p begin
            readlines(f)
            map(split(_, ":\t"))
            map(_[1] => _[2])
            Dict
        end
        cmdline = let
            f = dir / "cmdline"
            isfile(f) ? read(f, String) : ""
        end
        uids = @p begin
            dct["Uid"]
            split()
            map(parse(Int, _))
            NamedTuple{(:real, :effective, :saved, :filesystem)}()
        end
        gids = @p begin
            dct["Gid"]
            split()
            map(parse(Int, _))
            NamedTuple{(:real, :effective, :saved, :filesystem)}()
        end
        r = (
            pid = parse(Int, dct["Pid"]),
            name = dct["Name"],
            ppid = parse(Int, dct["PPid"]),
            uids,
            gids,
            state = dct["State"],
            cmdline,
            nthreads = parse(Int, dct["Threads"]),
        )
        @assert r.pid == pid  (pid, r.pid)
        r
    end
    
    rs = let
        f = dir / "stat"
        isfile(f) || return nothing
        str = readlines(f) |> only
        name = str[findfirst('(', str) + 1:findlast(')', str) - 1]
        fields = split(str[findlast(')', str) + 1:end])
        (
            name,
            pid = parse(Int, str[begin:findfirst('(', str) - 1]),
            state = fields[1],
            ppid = parse(Int, fields[2]),
            ttynr = parse(Int, fields[5]),
            utime = parse(Int, fields[12]),
            stime = parse(Int, fields[13]),
            children_utime = parse(Int, fields[14]),
            children_stime = parse(Int, fields[15]),
            create_time = parse(Int, fields[20]),
            cpu_num = parse(Int, fields[37]),
            blkio_ticks = parse(Int, fields[40]),
        )
    end
    @assert rs.pid == r.pid

    times = @p (
        user = rs.utime,
        system = rs.stime,
        # children_user = rs.children_utime,
        # children_system = rs.children_stime,
        iowait = rs.blkio_ticks
    ) |> map(_ / clock_ticks(hi)) |> CpuTimes()

    return (;
        r...,
        create_time = rs.create_time / clock_ticks(hi),
        rs.name, # this one?..
        rs.cpu_num,
        rs.state,
        times,
    )
end


function cpu_load_between(hia::HostInfo, hib::HostInfo)
    ncpu = length(cputimes(hib).individual)
    diff_total = cputimes(hib).total - cputimes(hia).total
    procs = @p begin
        pids(hib)
        map((old=process_info(hia, _), new=process_info(hib, _)))
        filter(!isnothing(_.old) && !isnothing(_.new))
        map((_.new.pid, _.new.name, _.new.cmdline, uid=_.new.uids.real, cpu=_.new.times - _.old.times))
        map((; @delete(_.cpu)..., used_frac=used_time(_.cpu) / total_time(diff_total) * ncpu))
    end
    used_frac_total = used_time(diff_total) / total_time(diff_total) * ncpu
    used_frac_accounted = @p procs |> sum(_.used_frac)
    push!(procs, (
        pid=-1, name="/other/", cmdline="", uid=0, used_frac=used_frac_total - used_frac_accounted
    ))
    (total=used_frac_total, processes=procs)
end
