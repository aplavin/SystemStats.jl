# SystemStats.jl

Retrieve system information from local or remote hosts, produce short realtime reports with process information, CPU and memory utilization. All of that — without installing anything on remotes.

![](https://raw.githubusercontent.com/aplavin/SystemStats.jl/master/test/report.png?token=GHSAT0AAAAAABOBNLBLVYSUGERYMLEHIHTYYPMA2MQ)

See the [Pluto notebook](https://aplavin.github.io/SystemStats.jl/test/notebook.html) for more details and examples.
