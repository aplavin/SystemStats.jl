### A Pluto.jl notebook ###
# v0.17.5

using Markdown
using InteractiveUtils

# ╔═╡ dc13be61-5012-498b-b99b-7226db56097b
using PlutoUI

# ╔═╡ ee7dbef7-dd8e-4c20-a715-17a35ea49b7d
using SystemStats

# ╔═╡ 8edea21e-75ff-11ec-3ac4-05b3e4eee98f
begin
	# import Pkg
	# eval(:(Pkg.develop(path=joinpath(@__DIR__, ".."))))
end

# ╔═╡ 769c30ed-24ff-4034-acc0-4a5533df1b3c
md"""
Load package first. `SystemStats.jl` exports some names, but not a lot.
"""

# ╔═╡ e347c78d-5456-4d58-bc74-75dfff2d6620
md"""
Gather information about a machine. Supports both the local machine (`"localhost"`) and SSH-accessible hosts.
"""

# ╔═╡ ef2fd092-3867-4ebc-b3da-eaec26a88dae
hi = retrieve_host_info("localhost")

# ╔═╡ c6a65807-f435-42e4-bec4-0e23f90c8ed5
md"""
This doesn't require anything specific to be installed on the remote machine: basically, `grep` is enough.

The `HostInfo` structure contains all the information needed. Further operations with it are offline and don't require connecting to the host again.
"""

# ╔═╡ 35e195ea-99cb-4691-9511-bb358d3dd8ad
md"""
Host information is retrieved as a bunch of virtual files, accessible with regular Julia filesystem functions:
"""

# ╔═╡ ce7f723a-5368-43dd-9503-42454a922cc8
readdir(joinpath(root(hi), "/"))

# ╔═╡ 9b0801d1-0c5c-41ff-8660-fff0378771dc
readdir(joinpath(root(hi), "/proc/"))

# ╔═╡ 3f21cfb7-cf3b-474c-808a-d2476faa2a19
readlines(joinpath(root(hi), "/proc/stat"))

# ╔═╡ 60a6158a-3c76-4e34-aff9-d06915fee339
md"""
`SystemStats.jl` provides some convenience functions, for example:

- Parse the `/etc/passwd` file into user entries
"""

# ╔═╡ d97e60a9-ec91-403a-af57-5acd336f09f2
users_by_uid(hi)

# ╔═╡ aa2a8a2a-4a33-4b92-a2f4-4b815ea9bf25
md"""
- Parse the `/proc/stat` file to extract cumulative CPU usage
"""

# ╔═╡ e1d881fe-ca4b-4a77-bef0-bb4bd9d35570
cputimes(hi)

# ╔═╡ ac3ce7af-04da-45f3-96c9-876556329c79
used_time(cputimes(hi).total)

# ╔═╡ d54b0c09-bacf-434f-b032-3457989dabe8
md"""
- List all `pid`s on the system
"""

# ╔═╡ 770bde59-1174-4084-8723-5ef822b1b9b7
SystemStats.pids(hi)

# ╔═╡ 8b1f9b09-5892-4869-988e-16973308f68a
md"""
- Extract details for a single process, or all processes at once
"""

# ╔═╡ e24a6c26-398d-4779-9832-e26150c4038e
SystemStats.process_info(hi, 1)

# ╔═╡ 3cdf9f1a-ff27-4e63-a1dd-834c3302f780
processes_info(hi)

# ╔═╡ 3e8a3be0-60f2-4696-88b9-05f9baf7972f
md"""
Computing CPU utilization requires multiple `HostInfo`s with some delay between them. Retrieve the second one:
"""

# ╔═╡ 59c56fcf-c971-4c31-9581-c3e530a21afb
hi2 = retrieve_host_info("localhost")

# ╔═╡ 9b04ff77-a80c-4a63-bf40-2e2551a73ecd
md"""
Compute CPU times differences:
"""

# ╔═╡ e8e0f9c3-2e77-4639-8642-423791d86944
cputimes(hi2).total - cputimes(hi).total

# ╔═╡ e1d9ee53-e66a-4f5f-a1c6-8a7abc62a832
md"""
Extract CPU usage for each process as a fraction (`1` means 100% of a single core):
"""

# ╔═╡ abe0a7bd-1f5a-45e9-a23b-78f223114b76
SystemStats.cpu_load_between(hi, hi2)

# ╔═╡ 21849a95-79f4-4544-9b44-d06c34dc60dd
md"""
Short reports can be displayed, showing memory utilization, and CPU utilization of top processes.

Use `SystemStats.print_colored(SystemStats.utilization_report(hi => hi2))` to print to the terminal. The result looks like this:
"""

# ╔═╡ 43a34f9d-2dea-41ce-9799-f6b9389bed1e
PlutoUI.LocalResource("report.png")

# ╔═╡ 17ab35be-b671-4041-b7c8-e60523515081
md"""
`SystemStats.jl` also provides a convenience function to monitor a couple of hosts continously, updating their utilization reports:\
`monitor_continously(["localhost", "host1", "host2", ...]; interval=2)`.

SSH multiplexing is recommended in this case, so that the same connections are reused.
"""

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
PlutoUI = "7f904dfe-b85e-4ff6-b463-dae2292396a8"
SystemStats = "f381a237-32a7-4d50-aa1c-abe0b395ff12"

[compat]
PlutoUI = "~0.7.30"
SystemStats = "~0.1.0"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.1"
manifest_format = "2.0"

[[deps.AbstractPlutoDingetjes]]
deps = ["Pkg"]
git-tree-sha1 = "8eaf9f1b4921132a4cff3f36a1d9ba923b14a481"
uuid = "6e696c72-6542-2067-7265-42206c756150"
version = "1.1.4"

[[deps.Accessors]]
deps = ["Compat", "CompositionsBase", "ConstructionBase", "Future", "LinearAlgebra", "MacroTools", "Requires", "Test"]
git-tree-sha1 = "2e427a6196c7aad4ee35054a9a90e9cb5df5c607"
uuid = "7d9f7c33-5ae7-4f3b-8dc6-eff91059b697"
version = "0.1.7"

[[deps.ArgTools]]
uuid = "0dad84c5-d112-42e6-8d28-ef12dabb789f"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.ColorTypes]]
deps = ["FixedPointNumbers", "Random"]
git-tree-sha1 = "024fe24d83e4a5bf5fc80501a314ce0d1aa35597"
uuid = "3da002f7-5984-5a60-b8a6-cbb66c0b333f"
version = "0.11.0"

[[deps.Compat]]
deps = ["Base64", "Dates", "DelimitedFiles", "Distributed", "InteractiveUtils", "LibGit2", "Libdl", "LinearAlgebra", "Markdown", "Mmap", "Pkg", "Printf", "REPL", "Random", "SHA", "Serialization", "SharedArrays", "Sockets", "SparseArrays", "Statistics", "Test", "UUIDs", "Unicode"]
git-tree-sha1 = "44c37b4636bc54afac5c574d2d02b625349d6582"
uuid = "34da2185-b29b-5c13-b0c7-acf172513d20"
version = "3.41.0"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"

[[deps.CompositionsBase]]
git-tree-sha1 = "455419f7e328a1a2493cabc6428d79e951349769"
uuid = "a33af91c-f02d-484b-be07-31d278c5ca2b"
version = "0.1.1"

[[deps.ConstructionBase]]
deps = ["LinearAlgebra"]
git-tree-sha1 = "f74e9d5388b8620b4cee35d4c5a618dd4dc547f4"
uuid = "187b0558-2788-49d3-abe0-74a17ed4e7c9"
version = "1.3.0"

[[deps.Crayons]]
git-tree-sha1 = "b618084b49e78985ffa8422f32b9838e397b9fc2"
uuid = "a8cc5b0e-0ffa-5ad4-8c14-923d3ee1735f"
version = "4.1.0"

[[deps.DataPipes]]
deps = ["Accessors", "SplitApplyCombine"]
git-tree-sha1 = "6da546248579d3084c06d1babd1431c64f981441"
uuid = "02685ad9-2d12-40c3-9f73-c6aeda6a7ff5"
version = "0.2.5"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.DelimitedFiles]]
deps = ["Mmap"]
uuid = "8bb1440f-4735-579b-a4ab-409b98df4dab"

[[deps.Dictionaries]]
deps = ["Indexing", "Random"]
git-tree-sha1 = "66bde31636301f4d217a161cabe42536fa754ec8"
uuid = "85a47980-9c8c-11e8-2b9f-f7ca1fa99fb4"
version = "0.3.17"

[[deps.Distributed]]
deps = ["Random", "Serialization", "Sockets"]
uuid = "8ba89e20-285c-5b6f-9357-94700520ee1b"

[[deps.Downloads]]
deps = ["ArgTools", "LibCURL", "NetworkOptions"]
uuid = "f43a241f-c20a-4ad4-852c-f6b1247861c6"

[[deps.FixedPointNumbers]]
deps = ["Statistics"]
git-tree-sha1 = "335bfdceacc84c5cdf16aadc768aa5ddfc5383cc"
uuid = "53c48c17-4a7d-5ca2-90c5-79b7896eea93"
version = "0.8.4"

[[deps.Future]]
deps = ["Random"]
uuid = "9fa8497b-333b-5362-9e8d-4d0656e87820"

[[deps.Hyperscript]]
deps = ["Test"]
git-tree-sha1 = "8d511d5b81240fc8e6802386302675bdf47737b9"
uuid = "47d2ed2b-36de-50cf-bf87-49c2cf4b8b91"
version = "0.0.4"

[[deps.HypertextLiteral]]
git-tree-sha1 = "2b078b5a615c6c0396c77810d92ee8c6f470d238"
uuid = "ac1192a8-f4b3-4bfe-ba22-af5b92cd3ab2"
version = "0.9.3"

[[deps.IOCapture]]
deps = ["Logging", "Random"]
git-tree-sha1 = "f7be53659ab06ddc986428d3a9dcc95f6fa6705a"
uuid = "b5f81e59-6552-4d32-b1f0-c071b021bf89"
version = "0.2.2"

[[deps.Indexing]]
git-tree-sha1 = "ce1566720fd6b19ff3411404d4b977acd4814f9f"
uuid = "313cdc1a-70c2-5d6a-ae34-0150d3930a38"
version = "1.1.1"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.IterTools]]
git-tree-sha1 = "fa6287a4469f5e048d763df38279ee729fbd44e5"
uuid = "c8e1da08-722c-5040-9ed9-7db0dc04731e"
version = "1.4.0"

[[deps.JSON]]
deps = ["Dates", "Mmap", "Parsers", "Unicode"]
git-tree-sha1 = "8076680b162ada2a031f707ac7b4953e30667a37"
uuid = "682c06a0-de6a-54ab-a142-c8b1cf79cde6"
version = "0.21.2"

[[deps.LibCURL]]
deps = ["LibCURL_jll", "MozillaCACerts_jll"]
uuid = "b27032c2-a3e7-50c8-80cd-2d36dbcbfd21"

[[deps.LibCURL_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll", "Zlib_jll", "nghttp2_jll"]
uuid = "deac9b47-8bc7-5906-a0fe-35ac56dc84c0"

[[deps.LibGit2]]
deps = ["Base64", "NetworkOptions", "Printf", "SHA"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[deps.LibSSH2_jll]]
deps = ["Artifacts", "Libdl", "MbedTLS_jll"]
uuid = "29816b5a-b9ab-546f-933c-edad1886dfa8"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[deps.MacroTools]]
deps = ["Markdown", "Random"]
git-tree-sha1 = "3d3e902b31198a27340d0bf00d6ac452866021cf"
uuid = "1914dd2f-81c6-5fcd-8719-6d5c9610ff09"
version = "0.5.9"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"

[[deps.Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[deps.MozillaCACerts_jll]]
uuid = "14a3606d-f60d-562e-9121-12d972cd8159"

[[deps.NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"

[[deps.Parsers]]
deps = ["Dates"]
git-tree-sha1 = "92f91ba9e5941fc781fecf5494ac1da87bdac775"
uuid = "69de0a69-1ddd-5017-9359-2bf0b02dc9f0"
version = "2.2.0"

[[deps.Pkg]]
deps = ["Artifacts", "Dates", "Downloads", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "Serialization", "TOML", "Tar", "UUIDs", "p7zip_jll"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"

[[deps.PlutoUI]]
deps = ["AbstractPlutoDingetjes", "Base64", "ColorTypes", "Dates", "Hyperscript", "HypertextLiteral", "IOCapture", "InteractiveUtils", "JSON", "Logging", "Markdown", "Random", "Reexport", "UUIDs"]
git-tree-sha1 = "5c0eb9099596090bb3215260ceca687b888a1575"
uuid = "7f904dfe-b85e-4ff6-b463-dae2292396a8"
version = "0.7.30"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.PyFormattedStrings]]
deps = ["Printf"]
git-tree-sha1 = "427aadbcb003bd2b39c87caa5177dded65f81ddf"
uuid = "5f89f4a4-a228-4886-b223-c468a82ed5b9"
version = "0.1.10"

[[deps.REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets", "Unicode"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.Reexport]]
git-tree-sha1 = "45e428421666073eab6f2da5c9d310d99bb12f9b"
uuid = "189a3867-3050-52da-a836-e630ba90ab69"
version = "1.2.2"

[[deps.Requires]]
deps = ["UUIDs"]
git-tree-sha1 = "838a3a4188e2ded87a4f9f184b4b0d78a1e91cb7"
uuid = "ae029012-a4dd-5104-9daa-d747884805df"
version = "1.3.0"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.SharedArrays]]
deps = ["Distributed", "Mmap", "Random", "Serialization"]
uuid = "1a1011a3-84de-559e-8e89-a11a2f7dc383"

[[deps.Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[deps.SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[deps.SplitApplyCombine]]
deps = ["Dictionaries", "Indexing"]
git-tree-sha1 = "dec0812af1547a54105b4a6615f341377da92de6"
uuid = "03a91e81-4c3e-53e1-a0a4-9c0c8f19dd66"
version = "1.2.0"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[deps.SystemStats]]
deps = ["Accessors", "Crayons", "DataPipes", "IterTools", "PyFormattedStrings", "SplitApplyCombine"]
path = "/home/aplavin/.julia/dev/SystemStats/test/.."
uuid = "f381a237-32a7-4d50-aa1c-abe0b395ff12"
version = "0.1.0"

[[deps.TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"

[[deps.Tar]]
deps = ["ArgTools", "SHA"]
uuid = "a4e569a6-e804-4fa4-b0f3-eef7a1d5b13e"

[[deps.Test]]
deps = ["InteractiveUtils", "Logging", "Random", "Serialization"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"

[[deps.nghttp2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850ede-7688-5339-a07c-302acd2aaf8d"

[[deps.p7zip_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "3f19e933-33d8-53b3-aaab-bd5110c3b7a0"
"""

# ╔═╡ Cell order:
# ╟─8edea21e-75ff-11ec-3ac4-05b3e4eee98f
# ╟─dc13be61-5012-498b-b99b-7226db56097b
# ╟─769c30ed-24ff-4034-acc0-4a5533df1b3c
# ╠═ee7dbef7-dd8e-4c20-a715-17a35ea49b7d
# ╟─e347c78d-5456-4d58-bc74-75dfff2d6620
# ╠═ef2fd092-3867-4ebc-b3da-eaec26a88dae
# ╟─c6a65807-f435-42e4-bec4-0e23f90c8ed5
# ╟─35e195ea-99cb-4691-9511-bb358d3dd8ad
# ╠═ce7f723a-5368-43dd-9503-42454a922cc8
# ╠═9b0801d1-0c5c-41ff-8660-fff0378771dc
# ╠═3f21cfb7-cf3b-474c-808a-d2476faa2a19
# ╟─60a6158a-3c76-4e34-aff9-d06915fee339
# ╠═d97e60a9-ec91-403a-af57-5acd336f09f2
# ╟─aa2a8a2a-4a33-4b92-a2f4-4b815ea9bf25
# ╠═e1d881fe-ca4b-4a77-bef0-bb4bd9d35570
# ╠═ac3ce7af-04da-45f3-96c9-876556329c79
# ╟─d54b0c09-bacf-434f-b032-3457989dabe8
# ╠═770bde59-1174-4084-8723-5ef822b1b9b7
# ╟─8b1f9b09-5892-4869-988e-16973308f68a
# ╠═e24a6c26-398d-4779-9832-e26150c4038e
# ╠═3cdf9f1a-ff27-4e63-a1dd-834c3302f780
# ╟─3e8a3be0-60f2-4696-88b9-05f9baf7972f
# ╠═59c56fcf-c971-4c31-9581-c3e530a21afb
# ╟─9b04ff77-a80c-4a63-bf40-2e2551a73ecd
# ╠═e8e0f9c3-2e77-4639-8642-423791d86944
# ╟─e1d9ee53-e66a-4f5f-a1c6-8a7abc62a832
# ╠═abe0a7bd-1f5a-45e9-a23b-78f223114b76
# ╟─21849a95-79f4-4544-9b44-d06c34dc60dd
# ╟─43a34f9d-2dea-41ce-9799-f6b9389bed1e
# ╟─17ab35be-b671-4041-b7c8-e60523515081
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
