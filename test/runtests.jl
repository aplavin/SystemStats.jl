using SystemStats
using Test

@testset begin
    hi = retrieve_host_info("localhost")

    @test gethostname(hi) == "localhost"
    @test SystemStats.clock_ticks(hi) == 100
    @test readdir(root(hi) / "/") == ["proc", "etc"]
    @test readdir(root(hi) / "/etc/") == ["passwd"]
    @test startswith(readlines(root(hi) / "/proc/stat")[1], "cpu ")

    @test users_by_uid(hi)[0].name == "root"

    cput = cputimes(hi)
    @test length(cput.individual) == length(Sys.cpu_info())
    @test used_time(cput.total) > 0
    @test all(>=(0), used_time.(cput.individual))
    @test used_time(cput.total) + idle_time(cput.total) ≈ total_time(cput.total)

    pii = processes_info(hi)
    @test all(p -> p.pid isa Int, pii)
    @test all(p -> p.name isa AbstractString, pii)
    @test all(p -> used_time(p.times) >= 0, pii)

    hi2 = retrieve_host_info("localhost")
    cpudiff = SystemStats.cpu_load_between(hi, hi2)
    @test cpudiff.total >= 0

    @test all(>=(0), meminfo(hi))
end


import CompatHelperLocal as CHL
CHL.@check()
